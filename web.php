[1mdiff --cc routes/web.php[m
[1mindex b9c266c,f432273..0000000[m
mode 100644,100755..100755[m
[1m--- a/routes/web.php[m
[1m+++ b/routes/web.php[m
[36m@@@ -11,9 -11,16 +11,23 @@@[m
  |[m
  */[m
  [m
[31m- Route::get('/', function () {[m
[31m-     return view('welcome');[m
[31m- });[m
[31m- Route::get('/contact', function () {[m
[31m-     return view('contact');[m
[31m- });[m
[31m -Route::get('/', 'ContactController@index')->name('index');[m
[32m+ [m
[32m+ [m
[32m++[m
[32m++Route::get('/', 'ContactController@index')->name('index');[m
[32m++Route::get('/contact', 'ContactController@getContact')->name('contact');[m
[32m++[m
[32m+ Route::get('/list', 'ContactController@list')->name('list');[m
[32m+ [m
[32m+ Route::get('/{contacts}/detail', 'ContactController@detail')->name('detail');[m
[32m+ [m
[32m+ Route::get('/filterbygrade','ContactController@filterByGrade')->name('filterByGrade');[m
[32m+ [m
[32m+ Route::get('/filterbystatus','ContactController@filterByStatus')->name('filterByStatus');[m
[32m+ [m
[32m+ Route::get('{$id}/destroy','ContactController@destroy')->name('destroy');[m
[32m+ [m
[32m++[m
[32m++[m
[32m++[m
[32m++[m
