$(document).ready(function(){           
    $(".grade").hide(); 
    $(".select-student").hide();

    $("#target1").click(function(){
        $(".grade").hide().find('input[type=radio]').prop('checked',false);
        $(".selectOne").parent().empty();  
        $(".select-student").hide().find('input[type=hidden]').val("");
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false); 
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);           
    });

    $("#target2").click(function(){
        $(".grade").show();
        $("#grade10").prop('checked',true);
        $("#allStudentGrade10").prop('checked',true);
        $(".select-student").hide()          
        $(".selectOne").parent().empty();  
        $(".selection10").hide();
        $(".selection11").hide();
        $(".selection12").hide();

        if($('#grade10').is(":checked")){
            $(".selection11").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").hide().find('input[type=radio]').prop('checked',false);
            $(".selection10").show();
        }

        if($('#grade11').is(":checked")){
            $(".selection10").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").hide().find('input[type=radio]').prop('checked',false);
            $(".selection11").show();
        }

        if($('#grade12').is(":checked")){
            $(".selection10").hide().find('input[type=radio]').prop('checked',false);
            $(".selection11").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").show(); 
        }
    });
    if($('#target1').is(':checked')) {
        $(".grade").hide().find('input[type=radio]').prop('checked',false);
        $(".selectOne").parent().empty();  
        $(".select-student").hide().find('input[type=hidden]').val("");
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false); 
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false); 
    }
    if($('#target2').is(':checked')){
        $(".grade").show();  
        $(".select-student").hide()          
        $(".selectOne").parent().empty();  
        $(".selection10").hide();
        $(".selection11").hide();
        $(".selection12").hide();

        if($('#grade10').is(":checked")){
            $(".selection11").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").hide().find('input[type=radio]').prop('checked',false);
            $(".selection10").show(); 
        }

        if($('#grade11').is(":checked")){
            $(".selection10").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").hide().find('input[type=radio]').prop('checked',false);
            $(".selection11").show();
        }

        if($('#grade12').is(":checked")){
            $(".selection10").hide().find('input[type=radio]').prop('checked',false);
            $(".selection11").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").show(); 
        }
    };

    $("#target3").click(function(){
        $(".select-student").show();
        $(".grade").hide().find('input[type=radio]').prop('checked',false);
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });
    if($('#target3').is(':checked')){
        $(".select-student").show();
    }
    
    // grade             
    $(document).on('click','#grade10', function(){
        $(".selection11").hide().find('input[type=radio]').prop('checked',false);
        $(".selection12").hide().find('input[type=radio]').prop('checked',false);
        $(".selection10").show();
        $("#allStudentGrade10").prop('checked',true);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
     
    });    

    $(document).on('click','#grade11', function(){
        $(".selection10").hide().find('input[type=radio]').prop('checked',false);
        $(".selection12").hide().find('input[type=radio]').prop('checked',false);
        $(".selection11").show();
        $("#allStudentGrade11").prop('checked',true);  
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });

    $(document).on('click','#grade12', function(){
        $(".selection10").hide().find('input[type=radio]').prop('checked',false);
        $(".selection11").hide().find('input[type=radio]').prop('checked',false);
        $(".selection12").show();
        $("#allStudentGrade12").prop('checked',true); 
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });

    $(".frmFunction").hide();
    $("#use").click(function(){
        $(".frmFunction").show();
        $("#answerType1").prop('checked',true);
        $("#any").prop('checked',true);
    });
    if($('#use').is(':checked')){
        $(".frmFunction").show();
        $(".form_status").show();
    };
    $("#notuse").click(function(){  
        $(".frmFunction").find('input[type=text]').val('');
        $(".frmFunction").find('input[type=radio]').prop('checked',false);
        $(".frmFunction").hide();                     
    });

    //HRdesignation
    $('.HRdesignation10').hide();
    $('.HRdesignation11').hide();
    $('.HRdesignation12').hide();
    $(document).on('click', '#allStudentGrade10', function(){
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);            
    });
    $(document).on('click', '#allStudentGrade11', function(){
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);            
    });
    $(document).on('click', '#allStudentGrade12', function(){
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);            
    });
    $(document).on('click', '#HR10', function(){
        $('.HRdesignation10').show();
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });
    $(document).on('click', '#HR11', function(){
        $('.HRdesignation11').show();
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });
    $(document).on('click', '#HR12', function(){
        $('.HRdesignation12').show();
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
    });
    if($('#HR10').is(":checked")){
        $('.HRdesignation10').show();
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        }
    if($('#HR11').is(":checked")){
        $('.HRdesignation11').show();
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        }
    if($('#HR12').is(":checked")){
        $('.HRdesignation12').show();
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        }

    for (let i = 0; i < $(".survey").val(); i++){
        var check = $('input[name="answerType'+i+'"]:checked').val();
        if (check === "Multiple Question") {
            $(".choice"+i).show();
            $(".choice_label"+i).show();
            $(".choice_button"+i).show();
        } else {
            $(".choice"+i).hide();
            $(".choice_label"+i).hide();
            $(".choice_button"+i).hide();
        }
    }

    $(document).on('click', '.answerType', function(){
        var value = $(this).val();
        var key = $(this).attr('data-num');
        if (value === "Multiple Question") {
            $(".choice"+key).show();
            $(".choice_label"+key).show();
            $(".choice_button"+key).show();
        } else {
            $(".choice"+key).hide();
            $(".choice_label"+key).hide();
            $(".choice_button"+key).hide();
        }
    })
});