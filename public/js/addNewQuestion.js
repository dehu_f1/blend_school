    var j = 0;
        $('#addQuestion').click(function(){
            let index = $('.addChoice').length;
            let num = $('.answerType').length;
            form = '<div id ="rowQueq'+j+'"><hr>';
            form +='<div class="frmFunction" id="frmFunction'+j+'"><input type="hidden" name="newsurvey[]" value='+j+'><div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Question* </b></label><div class="col-sm-8"><input required id="question'+j+'" type="text" name="newquestion'+j+'" class="form-control" ></div></div>';            
            form +='<div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Answer Type </b></label>&nbsp';
            form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="newanswerType'+j+'"  id="answerType1'+j+'" value="Yes/No Question" checked><label for="answerType1'+j+'" class="form-check-label" >Yes/No Selection</label></div>';
            form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="newanswerType'+j+'" id="answerType2'+j+'" value="Short Answer" ><label for="answerType2'+j+'" class="form-check-label" >Short Question</label></div>';
            form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="newanswerType'+j+'" id="answerType3'+j+'" value="Long Answer"><label for="answerType3'+j+'" class="form-check-label" >Short Selection</label></div>';
            form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="newanswerType'+j+'" id="answerType4'+j+'" value="Multiple Question"><label for="answerType4'+j+'" class="form-check-label" >Multiple Question*</label></div></div>';
            form +='<div class="form-group row frmFunction choice'+num+'"><input type="hidden" name="checknewchoice[]" value='+j+'><label class="col-sm-2 col-form-label choice'+num+'"><b> Choice* </b></label><div class="col-sm-8" ><input id="choice" type="text" name="newchoice'+j+'[]" class="form-control choice'+num+' width-item" maxlength="50" ><br><input id="choice2'+j+'" type="text" name="newchoice'+j+'[]" class="form-control choice'+num+' width-item" maxlength="50"><br>';
            form +='<div class="dynamic_field_' + index + ' choice'+num+'"></div><button type="button" class="btn btn-warning choice'+num+' addChoice" data-id='+index+'>Add a new choice </button></div></div>';
            form +='<div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Answer Required* </b></label>&nbsp<div class="form-check form-check-inline"> <input class="form-check-input" type="radio" name="newrequired'+j+'" id="any'+j+'" checked value="0"><label for="any'+j+'" class="form-check-label" >Any</label></div>';
            form +='<div class="form-check form-check-inline"><input class="form-check-input" type="radio" name="newrequired'+j+'" id="required'+j+'" value="1"><label for="required'+j+'" class="form-check-label" >Required</label> </div></div></div>';                           
            form +='<a style="font-size:30px" class="removeQuestion1"><span class="glyphicon glyphicon-minus-sign"></span></a></div>';                                      
            j++;                    
            $('#newQuestion').append(form);

            if($('.answerType').is(":checked")){
                $(".choice"+num).hide();
            }else{
                $(".choice"+num).hide();
            }
        });

        $(document).on('click','.removeQuestion1', function(){
            $(this).parent('div').remove();
        });
