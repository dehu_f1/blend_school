$(document).ready(function(){
    $(".grade").hide();
    $(".select-student").hide();

    $("#target1").click(function(){
        $(".grade").hide().find('input[type=radio]').prop('checked',false);
        $(".selectOne").parent().empty();
        $(".select-student").hide().find('input[type=hidden]').val("");
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });

    $("#target2").click(function(){
        $(".grade").show();
        $(".select-student").hide()
        $(".selectOne").parent().empty();
        $(".selection10").hide();
        $(".selection11").hide();
        $(".selection12").hide();

        if($('#grade10').is(":checked")){
            $(".selection11").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").hide().find('input[type=radio]').prop('checked',false);
            $(".selection10").show();
        }

        if($('#grade11').is(":checked")){
            $(".selection10").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").hide().find('input[type=radio]').prop('checked',false);
            $(".selection11").show();
        }

        if($('#grade12').is(":checked")){
            $(".selection10").hide().find('input[type=radio]').prop('checked',false);
            $(".selection11").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").show();
        }
    });
    $("#target3").click(function(){
        $(".select-student").show();
        $(".grade").hide().find('input[type=radio]').prop('checked',false);
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });

    // grade
    $(document).on('click','#grade10', function(){
        $(".selection11").hide().find('input[type=radio]').prop('checked',false);
        $(".selection12").hide().find('input[type=radio]').prop('checked',false);
        $(".selection10").show();
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);

    });

    $(document).on('click','#grade11', function(){
        $(".selection10").hide().find('input[type=radio]').prop('checked',false);
        $(".selection12").hide().find('input[type=radio]').prop('checked',false);
        $(".selection11").show();
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });

    $(document).on('click','#grade12', function(){
        $(".selection10").hide().find('input[type=radio]').prop('checked',false);
        $(".selection11").hide().find('input[type=radio]').prop('checked',false);
        $(".selection12").show();
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });


    $(".frmFunction").hide();
    $("#use").click(function(){
        $(".frmFunction").show();
    });
    $("#notuse").click(function(){
        $(".frmFunction").find('input[type=text]').val('');
        $(".frmFunction").find('input[type=radio]').prop('checked',false);
        $(".frmFunction").hide();
    });

    //HRdesignation
    $('.HRdesignation10').hide();
    $('.HRdesignation11').hide();
    $('.HRdesignation12').hide();
    $(document).on('click', '#allStudentGrade10', function(){
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });
    $(document).on('click', '#allStudentGrade11', function(){
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });
    $(document).on('click', '#allStudentGrade12', function(){
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });
    $(document).on('click', '#HR10', function(){
        $('.HRdesignation10').show();
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });
    $(document).on('click', '#HR11', function(){
        $('.HRdesignation11').show();
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
    });
    $(document).on('click', '#HR12', function(){
        $('.HRdesignation12').show();
        $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
        $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
    });


    let num = $(".answerType").data('num');
    $(".choice"+num).hide();
    $(document).on('click', '.answerType', function() {
        let num = $(this).data('num');
        $(".choice"+num).hide().find('input[type=text]').val('');
    });
    $(document).on('click', '.answerType4', function() {
        let num = $(this).data('num');
        $(".choice"+num).show();
    })

    // add new input
    $(document).on('click', '.addChoice', function(){
        let index = $(this).data('id');
        html = '<div>';
        html += '<input type="text" name="choice'+index+'[]" class="form-control choice-list" maxlength="50">';
        html += '<button type="button" class="btn btn-danger btn-remove" id="remove">X</button>'
        html += '</div>';
        $('.dynamic_field_' + index).append(html);
    });

    $(document).on('click', '.btn-remove', function(){
        $(this).parent('div').remove();
    });

    // add new form function
    var j = $('.number').val();

    $('#addQuestion').click(function(){
        let index = $('.addChoice').length;
        let num = $('.answerType').length;
        form = '<div><hr class="frmFunction">';
        form +='<div class="frmFunction" id="frmFunction'+j+'"><div class="form-group row frmFunction" >'
        form += '<label class="col-sm-2 col-form-label" ><b>Question* </b></label><div class="col-sm-8">'
        form +='<input id="question'+j+'" type="text" name="question'+j+'" value="{{ old("question'+j+'") }}" class="form-control" ></div></div>';
        form +='<div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Answer Type </b></label>&nbsp';
        form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="answerType'+j+'"  id="answerType1'+j+'" value="Yes/No Question"><label for="answerType1'+j+'" class="form-check-label" >Yes/No Selection</label></div>';
        form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="answerType'+j+'" id="answerType2'+j+'" value="Short Answer" ><label for="answerType2'+j+'" class="form-check-label" >Short Answer</label></div>';
        form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="answerType'+j+'" id="answerType3'+j+'" value="Long Answer"><label for="answerType3'+j+'" class="form-check-label" >Long Answer</label></div>';
        form +='<div class="form-check form-check-inline"><input class="form-check-input answerType4" data-num='+num+' type="radio" name="answerType'+j+'" id="answerType4'+j+'" value="Multiple Question"><label for="answerType4'+j+'" class="form-check-label" >Multiple Question</label></div></div>';
        form +='<div class="form-group row frmFunction choice'+num+'"><label class="col-sm-2 col-form-label choice'+num+'"><b> Choice* </b></label><div class="col-sm-8" ><input required id="choice" type="text" name="choice'+j+'[]" value="{{ old("choice'+j+'[]") }}" class="form-control choice'+num+'" maxlength="50"><br><input required id="choice2'+j+'" type="text" name="choice'+j+'[]" value="{{ old("choice'+j+'[]") }}" class="form-control choice'+num+'" maxlength="50"><br>';
        form +='<div class="dynamic_field_' + index + ' choice'+num+'"></div><button type="button" class="btn btn-warning choice'+num+' addChoice" data-id='+index+'>Add a new choice </button></div></div>';
        form +='<div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Answer Required* </b></label>&nbsp<div class="form-check form-check-inline"> <input class="form-check-input" type="radio" name="required'+j+'" id="any'+j+'"  value="0"><label for="any'+j+'" class="form-check-label" >Any</label></div>';
        form +='<div class="form-check form-check-inline"><input class="form-check-input" type="radio" name="required'+j+'" id="required'+j+'" value="1"><label for="required'+j+'" class="form-check-label" >Required</label> </div><button type="button" class="btn btn-danger frmFunction removeQuestion">X</button></div></div>';
        form +='</div>';
        j++;
        $('.number').val(j);
        $('#newQuestion').append(form);

        if($('.answerType').is(":checked")){
            $(".choice"+num).hide().find('input[type=text]').val('');
        }else{
            $(".choice"+num).hide().find('input[type=text]').val('');
        }
    });

    $(document).on('click','.removeQuestion', function(){
        $(this).parent('div').parent('div').parent('div').remove();
    });        
});


//show classes list by class
$(document).on('click', '.classesList', function() {
    var classname = $(this).attr('id');
    $.ajax({
        url: '{{ route("ajaxStudent") }}',
        method: 'get',
        data: {
            name: classname,
        },
        success: function(response) {
            $("#tbody").html("");
            response.forEach(function(value){
                studentlist = "<tr class='student" + value.id + "' >"+
                    "<td>" + value.name + "</td>"+
                    "<td>" + value.dob + "</td>"+
                    "<td>" + value.parent + "</td>"+
                    "<td>" + value.parent_email + "</td>"+
                    "<td>" + value.class_name + "</td>"+
                    "<td>" + '<input type="button" id="'+ value.id +'" class="btn btn-primary selectOneStudent" value="Select">' + "</td>"+
                    "</tr>";
                    $("#tbody").append(studentlist);
            });
            $('#studentsListModal').modal('show');
            return;
        },
        error: function() {

        }
    });
});

// show students
$(document).on('click', '.selectOneStudent', function(){
    $(this).hide();
    var id_student = $(this).attr('id');
    $.ajax({
        url: '{{ route("SelectOneStudent") }}',
        method: 'get',
        data: {
            id: id_student,
        },
        success: function(response) {
            response.forEach(function(value){
                student = '<div class="form-group row select-student selectOne" >'
                student += '<label class="col-sm-2 col-form-label select-student"></label>'
                student += '<input type="hidden" name="showStudent[]" value="'+value.name+'">'
                student += '<div class="col-sm-6 "><div  class="select-student ">'+value.name+'</div></div>'
                student += '<button type="button" class="btn btn-danger btn-removeStudent  select-student" >Delete</button></div>'
                $(".showStudent").append(student);
            });
            $('#studentsListModal').modal('show');
            return;
        },
        error: function() {

        }
    });
});

$(document).on('click','.btn-removeStudent', function(){
    $(this).parent('div').remove();
});

//datepicker
$(function () {
    var date = new Date();
    date.setDate(date.getDate());
    $('.datepicker').datepicker({ 
        startDate: date
    });
});

//button loading
$(document).on('click','#submit', function(){
    $("#loader").show();
    $("#cancel").hide();
});
