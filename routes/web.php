<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactController@index')->name('index');

Route::get('/contact', 'ContactController@getContact')->name('contact');

Route::post('/addContact', 'ContactController@addContact')->name('addContact');

Route::post('/addContact', 'ContactController@addContact')->name('addContact');

Route::get('/ajax-student', 'ContactController@ajaxStudent')->name('ajaxStudent');

Route::get('/SelectOneStudent', 'ContactController@ajaxSelectOneStudent')->name('SelectOneStudent');

Route::get('/list', 'ContactController@list')->name('list');

Route::get('/{id}/detail', 'ContactController@detail')->name('detail');

Route::get('/filter', 'ContactController@filter')->name('filter');

Route::get('{id}/destroy','ContactController@destroy')->name('destroy');

Route::get('/edit/{contact}', 'ContactController@edit')->name('edit');

Route::patch('/update/{contact}', 'ContactController@update')->name('update');

Route::get('store','ContactController@store')->name('store');

Route::post('/deleteName','ContactController@deleteName')->name('deleteName');

Route::get('deletesurvey/{survey}','SurveyController@delete')->name('survey.delete');

Route::get('deletechoice/{choice}','ChoiceController@delete')->name('choice.delete');

Route::get('/detail/send','ContactController@sendMail')->name('sendMail');


