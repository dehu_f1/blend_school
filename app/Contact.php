<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable =
    [
        'target',
        'grade',
        'register',
        'registerdate',
        'title',
        'content',
        'student_name',
        'select_grade',
        'HR_designation',
        'form_status',
        'send_status',
        'attachfile1',
        'attachfile2',
        'attachfile3',
        'deadline'
    ];

    public function survey()
    {
        return $this->hasMany('App\Survey','contact_id','id');
    }

    public function student()
    {
        return $this->belongsToMany('App\Student','contact_students','contact_id','student_id');
    }
}
