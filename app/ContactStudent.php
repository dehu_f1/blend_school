<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactStudent extends Model
{
    protected $fillable = 
    [
        'contact_id', 
        'student_id'
    ];
}
