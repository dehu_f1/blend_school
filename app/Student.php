<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $fillable = 
    [   
        'name', 
        'dob', 
        'parent', 
        'parent_email', 
        'address', 
        'phone', 
        'grade', 
        'class_name'
    ];

    public function contact()
    {
        return $this->belongsToMany('App\Contact','contact_students','student_id','contact_id');
    }
}
