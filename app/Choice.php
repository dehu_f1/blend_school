<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Choice extends Model
{
    use SoftDeletes;

    protected $fillable = 
    [
        'option', 
        'survey_id'
    ];

    public function survey()
    {
        return $this->belongsTo('App\Survey','survey_id');
    }
}
