<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
    use SoftDeletes;

    protected $fillable =
    [
        'title',
        'type',
        'required',
        'contact_id'
    ];

    public function choice()
    {
        return $this->hasMany('App\Choice','survey_id','id');
    }
}
