<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validateCreateContact extends FormRequest
{
    private $selectTarget;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'target' => 'required',
            'date' => 'required',
            'date' => 'after:yesterday',
            'title' => 'required|max:100',
            'fulltext' => 'required|max:250',
            'attachfile1' => 'mimes:jpg,jpeg,png,gif',
            'attachfile2' => 'mimes:jpg,jpeg,png,gif',
            'attachfile3' => 'mimes:jpg,jpeg,png,gif',
        ];
    }
}
