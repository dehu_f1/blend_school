<?php

namespace App\Http\Controllers;
use App\Choice;

use Illuminate\Http\Request;

class ChoiceController extends Controller
{
    public function delete(Choice $choice)
    {
        $choice->delete();
        return redirect()->back(); 
    }
}
