<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;

class SurveyController extends Controller
{
    public function delete(Survey $survey)
    {
        $survey->delete();
        return redirect()->back(); 
    }
}
