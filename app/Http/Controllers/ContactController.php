<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\validateCreateContact;
use App\Http\Requests\UpdateContactRequest;
use App\Contact;
use App\Survey;
use App\Choice;
use App\Receiver;
use App\Student;
use DB;
use Mail;

class ContactController extends Controller
{
    public function index()
    {
        $direction = "Index";
        return view('index', compact('direction'));
    }

    public function getContact()
    {
        $direction = "Create contact";
        $classes = Student::orderBy('class_name')->get()->groupBy(function ($item) {
            return $item->class_name;
        });
        return view('contact', compact('classes', 'direction'));
    }

    public function addContact(validateCreateContact $request)
    {
        $contact = new Contact;
        $contact->target = $request->target;
        $contact->grade = $request->grade;
        $contact->select_grade = $request->selectTarget;
        if ($request->HRdesignation != null) {
            $contact->HR_designation = implode(", ", $request->HRdesignation);
        }
        if ($request->showStudent != null) {
            $contact->student_name = implode(", ", $request->showStudent);
        }
        $contact->registerdate = $request->date;
        $contact->title = $request->title;
        $contact->content = $request->fulltext;

        for ($i = 1; $i <= 3; $i++) {
            if ($request->hasFile('attachfile' . $i)) {
                $file = $request->file('attachfile' . $i);
                $newFile = date('Y-m-d-H-i-m') . '-' . $file->getClientOriginalName();
                $file->move('upload/file', $newFile);
                $contact->{'attachfile' . $i} = $newFile;
            }
        }
        $contact->send_status = $request->sent;
        $contact->form_status = $request->function;
        $contact->deadline = $request->deadline;
        $contact->save();

        for ($i = 0; $i < $request->number; $i++) {
            $survey = new Survey;
            $survey->title = $request->{'question' . $i};
            $survey->type = $request->{'answerType' . $i};
            $survey->required = $request->{'required' . $i};
            $survey->contact_id = $contact->id;
            $survey->save();

            foreach ($request->{'choice'.$i} as $key => $value) {
                Choice::create([
                    'survey_id' => $survey->id,
                    'option' => $value,
                ]);
            }
        }
        return redirect()->route('detail', $contact->id);
    }

    public function ajaxStudent(Request $request)
    {
        $students = Student::where('class_name', $request->all()['name'])->get();
        return response()->json($students);
    }

    public function ajaxSelectOneStudent(Request $request)
    {
        $student = Student::where('id', $request->all()['id'])->get();
        return response()->json($student);
    }

    public function list()
    {
        $direction = "Contact List [All]";
        $contacts = Contact::paginate(15);
        return view('list', compact('contacts', 'direction'));
    }

    public function detail($id)
    {
        $contacts = Contact::findorfail($id);
        $direction = "Contact Detail";
        return view('detail', compact('contacts', 'direction'));
    }

    private function _setConditionFilter($data)
    {
        $arr = [];

        if(isset($data['target'])){
            $arr['target'] = $data['target'];
        }
        if(isset($data['send_status'])){
            $arr['send_status'] = $data['send_status'];
        }
        return $arr;
    }

    public function filter(Request $request)
    {
        $data = $request->all();
        $condition = $this->_setConditionFilter($data);

        $contacts = Contact::where($condition)->paginate(15);
        $target_id = $request->input('target');
        $direction = "Contact list - Target [$target_id]";
        $send_statuses = ['Sent', 'Unsent'];
        return view('list', compact('contacts', 'direction', 'send_statuses', 'input_sendstatus', 'data'));

    }

    public function getMail($req){
        $sendMail = [];

        if ($req['target'] == 'All students') {
            $sendMail = Student::pluck('parent_email')->toArray();
        }
        if (isset($req['student_name'])) {
            $arr = explode(', ', $req['student_name']);
            $sendMail = Student::whereIn('name', $arr)->pluck('parent_email')->toArray();
        }
        if (isset($req['HR_designation'])) {
            $arr = explode(', ', $req['HR_designation']);
            $sendMail = Student::whereIn('class_name', $arr)->pluck('parent_email')->toArray();
        }
        if (isset($req['grade']) && empty($req['HR_designation'])) {
            $sendMail = Student::where('grade', $req['grade'])->pluck('parent_email')->toArray();
        }
        return $sendMail;
    }


    public function sendMail(Request $request)
    {
        $req = $request->toArray();
        $sendMail = $this->getMail($req);
        $contacts = Contact::where('id', $req['id'])->first();
        $data['contacts'] = $contacts;

        if (!empty($sendMail)) {
                Mail::send('emails.parents', $data,
                    function ($message) use ($sendMail) {
                        $message->subject('Notice from Blend School');
                        foreach ($sendMail as $send) {
                            $message->to($send);
                        }
                    });
                    $contacts->send_status = true;
        }else {
            $message = 'Email not exist';
            return "<script type='text/javascript'>alert('$message');</script>";
        };
        $contacts->save();
        return redirect()->back();
    }

    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();
        return redirect()->route('list')->with('success', 'Delete Success');
    }

    public function edit(Contact $contact)
    {
        $direction = "Edit Contact";
        $classes = Student::orderBy('class_name')->get()->groupBy(function ($item) {
            return $item->class_name;
        });
        $students = explode(',',$contact->student_name);
        $HRdesignations = explode(', ',$contact->HR_designation);
        $surveys = Survey::where('contact_id', $contact->id)->get();
        $surveysId = Survey::where('contact_id', $contact->id)->pluck('id')->toArray();
        $choices = Choice::whereIn('survey_id', $surveysId)->get()->groupBy('survey_id')->toArray();
        return view('edit', compact('contact', 'surveys', 'direction', 'choices', 'classes','students','HRdesignations'));
    }

    public function updateChoice($attribute, $key, $survey) {
        if(isset($attribute['choice_id' . $key])){
            foreach($attribute['choice_id' . $key] as $key_choice=>$choiceId){
                if (isset($attribute['choice' . $key . $key_choice])) {
                $choice = Choice::where([
                    'survey_id' => $survey->id,
                    'id' => $choiceId
                ]);
                $choice->update([
                    'option' => $attribute['choice' . $key . $key_choice],
                    'survey_id' => $survey->id,
                ]);
                }
            }
        }
    }

    public function createChoice($attribute, $key, $survey) {
        if (isset($attribute['choice' . $key])) {
            foreach($attribute['choice' . $key] as $index => $newchoice){
                $choice = new Choice();
                $choice->option = $newchoice;
                $choice->survey_id = $survey->id;
                $choice->save();
            }
        }
    }

    public function createChoiceInNewSurvey($attribute, $key, $survey) {
        foreach($attribute['newchoice' . $key] as $index => $newchoice){
            if(isset($newchoice)){
                Choice::create([
                    'survey_id' => $survey->id,
                    'option' => $newchoice,
                ]);
            }
        }
    }

    public function createSurvey($attribute, $contact) {
        if(isset($attribute['newsurvey'])){
            foreach($attribute['newsurvey'] as $key => $value){
                $survey = Survey::create([
                    'title' => $attribute['newquestion' .$value],
                    'type' => $attribute['newanswerType' .$value],
                    'required' => $attribute['newrequired' .$value],
                    'contact_id' => $contact->id,
                ]);
                $this->createChoiceInNewSurvey($attribute, $key, $survey);
            }
        }
    }

    public function updateAttachFile($attribute, $contact) {
        $oldfile1 = $contact->attachfile1;
        if (isset($attribute['attachfile1'])) {
            $file = $attribute['attachfile1'];
            $oldfile1 =  $file->getClientOriginalName();
            $destinationPath = public_path('navigave_layouts/file');
            $file->move($destinationPath, $oldfile1);
        }

        $oldfile2 = $contact->attachfile2;

        if (isset($attribute['attachfile2'])) {
            $file = $attribute['attachfile2'];
            $oldfile2 =  $file->getClientOriginalName();
            $destinationPath = public_path('navigave_layouts/file');
            $file->move($destinationPath, $oldfile2);
        }

        $oldfile3 = $contact->attachfile3;

        if (isset($attribute['attachfile3'])) {
            $file = $attribute['attachfile3'];
            $oldfile3 =  $file->getClientOriginalName();
            $destinationPath = public_path('navigave_layouts/file');
            $file->move($destinationPath, $oldfile3);
        }

        $contact->update([
            'attachfile1' => $oldfile1,
            'attachfile2' => $oldfile2,
            'attachfile3' => $oldfile3,
        ]);
    }

    public function updateSurvey($attribute) {
        if($attribute['form_status'] === '1') {
            if(isset($attribute['survey_id'])) {
                foreach($attribute['survey_id'] as $key=>$surveyId){
                    $survey = Survey::find($surveyId);
                        $survey->update([
                            'title' => $attribute['question' . $key],
                            'type' => $attribute['answerType'. $key],
                            'required' => $attribute['required' . $key],
                        ]);
                    $this->updateChoice($attribute, $key, $survey);
                    $this->createChoice($attribute, $key, $survey);
                }
            }
        }
    }

    public function updateContact($attribute, $contact) {
        if(isset($attribute['HR_designation'])){
            $attribute['HR_designation'] = (implode(", ", $attribute['HR_designation']));
        }
        if(isset($attribute['student_name'])){
            $attribute['student_name'] = (implode(", ", $attribute['student_name']));
        }
        $contact->update($attribute);
    }

    public function update(Contact $contact, UpdateContactRequest $request)
    {
        $attribute = request()->all();
        $this->updateAttachFile($attribute, $contact);
        $this->updateSurvey($attribute);
        $this->updateContact($attribute, $contact);
        $this->createSurvey($attribute, $contact);
        return redirect(route('list'))->with('success','Update Success');
    }
}
