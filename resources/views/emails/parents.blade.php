<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Parent Contact</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
        #table th {
            padding: 16px;

        }

        #table td {
            padding: 16px;

        }
    </style>
</head>

<body>
    <div class="container-fluid">

        
        <div class="mt-5">
            <table class="clearfix table table-bordered">
                <thead>
                    <tr>
                        <th class="col-xs-2 col-sm-2 col-md-2 col-lg-3" scope="col">Contact Number</th>
                        <td>{{$contacts->id}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Date</th>
                        <td>{{Carbon\Carbon::parse($contacts->registerdate)->diffForHumans()}}</td>

                    </tr>
                    <tr>
                        <th scope="col">Title</th>
                        <td>{{$contacts->title}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Send target</th>
                        <td><?php
                        if ($contacts->target == 'All students') {
                            echo($contacts->target);
                        }
                        if (isset($contacts->student_name)) {
                            echo($contacts->student_name);
                        }
                        if (isset($contacts->HR_designation)) {
                            echo($contacts->HR_designation);
                        }
                        if (isset($contacts->grade) && empty($contacts->HR_designation)) {
                            echo($contacts->grade);
                        }
                        ?></td>
                    </tr>
                    
                    <tr>
                        <th scope="col">Article</th>
                        <td>{{$contacts->content}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Attach file</th>
                        <td>
                            <?php
                            if(isset($contacts->attachfile1)) {
                                echo "<img style='width:25px' src='img/file.png'> &nbsp;";
                            }
                            if(isset($contacts->attachfile2)){
                                echo "<img style='width:25px' src='img/file.png'> &nbsp;";
                            }
                            if(isset($contacts->attachfile3)){
                                echo "<img style='width:25px' src='img/file.png'>";
                            };
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Register person</th>
                        <td>{{$contacts->register}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Register date</th>
                        <td>{{$contacts->registerdate}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Final Update</th>
                        <td>{{$contacts->updated_at}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Last Modified</th>
                        <td>{{$contacts->updated_at}}</td>
                    </tr>

                </thead>
            </table>
        </div>

        <br><br><br><br>

        @if($contacts->form_status)
        <div class="mt-5 p-3" style="background-color: darkgray">
            Form contents
        </div>
        <div class="mt-5">
            <table class="clearfix table table-bordered">
                <thead>
                    <tr>
                        <th class="col-xs-2 col-sm-2 col-md-2 col-lg-3" scope="col">Deadline</th>
                        <td>{{$contacts->deadline}}</td>
                    </tr>
                    @foreach($contacts->survey as $survey)
                    @if($survey->type == 'Yes/No Question')
                    <tr>
                        <th scope="col">Question</th>
                        <td>
                            {{$survey->title}} <br>
                            <input type="radio" name="yesno" checked> Yes &nbsp;
                            <input type="radio" name="yesno"> No
                        </td>
                    </tr>
                    @elseif($survey->type == 'Long Answer')
                    <tr>
                        <th scope="col">Question</th>
                        <td>
                            {{$survey->title}} <br>
                            <textarea style="width:40%" colspan="4"> </textarea>
                        </td>
                    </tr>
                    @elseif($survey->type == 'Short Answer')
                    <tr>
                        <th scope="col">Question</th>
                        <td>
                            {{$survey->title}} <br>
                            <input style="width:40%" type="text">
                        </td>
                    </tr>
                    @elseif($survey->type == 'Multiple Question')
                    <tr>
                        <th scope="col">Question</th>
                        <td>
                            {{$survey->title}} <br>                            
                            @foreach($survey->choice as $choices)
                            <input type="checkbox" value="{{$choices->option}}">{{$choices->option}} &nbsp;
                            @endforeach
                        </td>
                    </tr>
                    @endif
                    @endforeach

                </thead>
            </table>
        </div>
        @endif
    </div>
</body>

</html>