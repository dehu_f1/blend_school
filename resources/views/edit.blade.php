@extends('layouts.master')
@section('title')
        BLEND | Create contact
@endsection

@section('content')
<link rel="stylesheet" href="css/thanh-style.css">
    <div class="container">
        <div class="row">
            <div class="col-12">            
                @if(Session::has('inform'))
                    <div class="alert alert-info">{{ Session::get('inform') }}</div>                
                @endif 

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif       

                <form method="post" action="{{route('update',$contact->id)}}" enctype="multipart/form-data" novalidate>
                    @csrf
                    @method('PATCH')
                    <div class="row" style="margin-bottom:40px">
                        <div class="col-xl-12">
                            <div class="form-group row" >
                                <label class="col-sm-2 col-form-label"><b>Send Target*: </b></label>&nbsp;
                                
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="target" id="target1" value="All students" {{$contact->target==="All students" ? "checked":""}}>
                                    <label class="form-check-label" for="target1">All</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="target" id="target2" value="Grade" {{$contact->target==="Grade" ? "checked":""}}>
                                    <label class="form-check-label" for="target2" >Grades</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="target" id="target3" value="Individual" {{$contact->target==="Individual" ? "checked":""}}>
                                    <label class="form-check-label" for="target3">Individual</label>
                                </div>
                            </div>

                            <div class="form-group row grade">
                                <label class="col-sm-2 col-form-label"><b>Grades </b></label>&nbsp;
                                
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="grade" id="grade10" value="Grade 10" {{$contact->grade === "Grade 10" ? "checked" : ""}}>
                                    <label class="form-check-label" for="grade10">Grade 10</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="grade" id="grade11" value="Grade 11" {{$contact->grade === "Grade 11" ? "checked" : ""}}>
                                    <label class="form-check-label" for="grade11">Grade 11</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="grade" id="grade12" value="Grade 12" {{$contact->grade === "Grade 12" ? "checked" : ""}}>
                                    <label class="form-check-label" for="grade12">Grade 12</label>
                                </div>
                            </div>
                            {{--  // select target 10  --}}
                            <div class="form-group row grade selectTarget selection10" id="selectTarget">
                                <label class="col-sm-2 col-form-label selection10"><b>Select Target </b></label>&nbsp;
                                
                                <div class="form-check form-check-inline selection10">
                                    <input class="form-check-input selection10" type="radio" name="select_grade" id="allStudentGrade10" value="All Student grade 10" {{$contact->select_grade === "All Student grade 10" ? "checked":""}}>
                                    <label class="form-check-label selection10" for="allStudentGrade10">All Student grade 10</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input selection10" type="radio" name="select_grade" id="HR10" value="HR designation 10" {{$contact->select_grade === "HR designation 10" ? "checked":""}}>
                                    <label class="form-check-label selection10" for="HR10">HR designation 10</label>
                                </div>
                            </div>

                            {{--  // select target 11  --}}
                            <div class="form-group row grade selectTarget selection11" id="selectTarget">
                                <label class="col-sm-2 col-form-label selection11"><b>Select Target </b></label>&nbsp;
                                
                                <div class="form-check form-check-inline selection11">
                                    <input class="form-check-input selection11" type="radio" name="select_grade" id="allStudentGrade11" value="All Student grade 11" {{$contact->select_grade === "All Student grade 11" ? "checked":""}}>
                                    <label class="form-check-label selection11" for="allStudentGrade11">All Student grade 11</label>
                                </div>
                                <div class="form-check form-check-inline selection11">
                                    <input class="form-check-input selection11" type="radio" name="select_grade" id="HR11" value="HR designation 11" {{$contact->select_grade === "HR designation 11" ? "checked":""}}>
                                    <label class="form-check-label selection11" for="HR11">HR designation 11</label>
                                </div>
                            </div>

                            {{--  // select target 12  --}}
                            <div class="form-group row grade selectTarget selection12" id="selectTarget">
                                <label class="col-sm-2 col-form-label selection12"><b>Select Target </b></label>&nbsp;
                                
                                <div class="form-check form-check-inline selection12">
                                    <input class="form-check-input selection12" type="radio" name="select_grade" id="allStudentGrade12" value="All Student grade 12" {{$contact->select_grade === "All Student grade 12" ? "checked":""}}>
                                    <label class="form-check-label selection12" for="allStudentGrade12">All Student grade 12</label>
                                </div>
                                <div class="form-check form-check-inline selection12">
                                    <input class="form-check-input selection12" type="radio" name="select_grade" id="HR12" value="HR designation 12" {{$contact->select_grade === "HR designation 12" ? "checked":""}}>
                                    <label class="form-check-label selection12" for="HR12">HR designation 12</label>
                                </div>
                            </div>

                            {{--  HR designation 10  --}}
                            <div class="form-group row HRdesignation10">
                                <label class="col-sm-2 col-form-label HRdesignation10"><b>HR designation </b></label>
                               
                                <div class="form-check form-check-inline HRdesignation10">
                                    <input class="form-check-input HRdesignation10" type="checkbox" name="HR_designation[]" id="class10A" value="Class 10A" {{in_array('Class 10A', $HRdesignations) ? "checked":""}}>&nbsp;
                                    <label class="form-check-label HRdesignation10" for="class10A">ClassName: 10A </label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation10">
                                    <input class="form-check-input HRdesignation10" type="checkbox" name="HR_designation[]" id="class10B" value="Class 10B" {{in_array('Class 10B', $HRdesignations) ? "checked":""}}>
                                    <label class="form-check-label HRdesignation10" for="class10B">ClassName: 10B</label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation10">
                                    <input class="form-check-input HRdesignation10" type="checkbox" name="HR_designation[]" id="class10C" value="Class 10C" {{in_array('Class 10C', $HRdesignations) ? "checked":""}}>
                                    <label class="form-check-label HRdesignation10" for="class10C">ClassName: 10C</label>
                                </div>
                            </div>

                           {{--  HR designation 11  --}}
                           <div class="form-group row HRdesignation11">
                                <label class="col-sm-2 col-form-label HRdesignation11"><b>HR designation </b></label>
                                
                                <div class="form-check form-check-inline HRdesignation11">
                                    <input class="form-check-input HRdesignation11" type="checkbox" name="HR_designation[]" id="class11A" value="Class 11A" {{in_array('Class 11A', $HRdesignations) ? "checked":""}}>&nbsp;
                                    <label class="form-check-label HRdesignation11" for="class11A">ClassName: 11A </label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation11">
                                    <input class="form-check-input HRdesignation11" type="checkbox" name="HR_designation[]" id="class11B" value="Class 11B" {{in_array('Class 11B', $HRdesignations) ? "checked":""}}>
                                    <label class="form-check-label HRdesignation11" for="class11B">ClassName: 11B</label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation11">
                                    <input class="form-check-input HRdesignation11" type="checkbox" name="HR_designation[]" id="class11C" value="Class 11C" {{in_array('Class 11C', $HRdesignations) ? "checked":""}}>
                                    <label class="form-check-label HRdesignation11" for="class11C">ClassName: 11C</label>
                                </div>
                            </div>
                            {{--  HR designation 12  --}}
                           <div class="form-group row HRdesignation12">
                                <label class="col-sm-2 col-form-label HRdesignation12"><b>HR designation </b></label>
                                
                                <div class="form-check form-check-inline HRdesignation12">
                                    <input class="form-check-input HRdesignation12" type="checkbox" name="HR_designation[]" id="class12A" value="Class 12A" {{in_array('Class 12A', $HRdesignations) ? "checked":""}}>&nbsp;
                                    <label class="form-check-label HRdesignation12" for="class12A">ClassName: 12A </label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation12">
                                    <input class="form-check-input HRdesignation12" type="checkbox" name="HR_designation[]" id="class12B" value="Class 12B" {{in_array('Class 12B', $HRdesignations) ? "checked":""}}>
                                    <label class="form-check-label HRdesignation12" for="class12B">ClassName: 12B</label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation12">
                                    <input class="form-check-input HRdesignation12" type="checkbox" name="HR_designation[]" id="class12C" value="Class 12C" {{in_array('Class 12C', $HRdesignations) ? "checked":""}}>
                                    <label class="form-check-label HRdesignation12" for="class12C">ClassName: 12C</label>
                                </div>
                            </div>
                            
                            <div class="form-group row select-student" id="select-student">
                                <label class="col-sm-2 col-form-label"><b>Select Student*</b></label>
                                <div class="col-sm-8">
                                    <button  type="button" name="select-student" class="btn btn-primary" data-toggle="modal" data-target="#classesListModal">Select Student</button>                                    
                                </div>
                            </div>

                            {{-- show Student --}}
                            <div class="showStudent" id="{{$contact->id}}">
                                @foreach($students as $key => $student)
                                    <div class="form-group row select-student selectOne"  >
                                        <label class="col-sm-2 col-form-label select-student"></label>
                                        <input type="hidden" name="student_name[]" value="{{$student}}">
                                        <div class="col-sm-6 "><div  class="select-student ">{{$student}}</div>
                                    </div>
                                <button type="button" id="{{$contact->id}}" data_index="{{$key}}" data_name="{{$student}}" class="btn btn-danger btn-removeStudent  select-student" >Delete</button></div>
                                @endforeach
                            </div>
                            {{-- --end show Student --}}

                            <div class="form-group row" >
                                <label class="col-sm-2 col-form-label" ><b>Date* </b></label>
                                <div id="datepicker" class="input-group date datepicker" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                    <input class="form-control" name="registerdate" value={{$contact->registerdate}} readonly>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                        
                            </div>

                            <div class="form-group row" >
                                <label class="col-sm-2 col-form-label" ><b>Title* </b></label>
                                <div class="col-sm-8">
                                    <input  id="title" type="text" name="title" class="form-control" value="{{$contact->title}}">
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label class="col-sm-2 col-form-label"><b>Full Text*</b></label>
                                <div class="col-sm-8">
                                <textarea name="content" id="fulltext" cols="30" rows="10" class="form-control">{{$contact->content}}</textarea>
                                </div>
                            </div>
                            
                            <div class="form-group row" >
                                <label class="col-sm-2 col-form-label"><b>Attach File*</b></label>
                                <div class="col-sm-8">
                                    {{$contact->attachfile1}}  <input type="file" class="form-control-file" id="inputFile" name="attachfile1"><br>
                                    {{$contact->attachfile2}}  <input type="file" class="form-control-file" id="inputFile" name="attachfile2"><br>
                                    {{$contact->attachfile3}}  <input type="file" class="form-control-file" id="inputFile" name="attachfile3"><br>
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label class="col-sm-2 col-form-label" ><b>Form functions </b></label>&nbsp
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="form_status" id="notuse" value="0" {{$contact->form_status === '0' ? "checked" : ""}} id="notuse">
                                    <label class="form-check-label" for="notuse" >Not Uses</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="form_status" id="use" value="1" {{$contact->form_status === '1' ? "checked" : ""}} id="use">
                                    <label class="form-check-label" for="use">Uses</label>
                                </div>
                            </div>

                            {{-- --show survey-- --}}
                            <input type="hidden" value="{{ $surveys->count() }}" class = "survey">
                            @foreach($surveys as $key => $survey)
                            <input type="hidden" name="survey_id[]" value="{{ $survey->id }}">
                            <div class="frmFunction" id="frmFunction">
                                <div class="form-group row frmFunction" >
                                    <label class="col-sm-2 col-form-label" ><b>Question* </b></label>
                                    <div class="col-sm-8">
                                        <input id="question" type="text" name="question{{$key}}" value="{{$survey->title}}" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group row frmFunction" >
                                    <label class="col-sm-2 col-form-label" ><b>Answer Type </b></label>&nbsp
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input answerType"  data-num='{{$key}}' type="radio" name="answerType{{$key}}"  id="answerType1" value="Yes/No Question" {{$survey->type === "Yes/No Question" ? "checked":""}}>
                                        <label class="form-check-label" for="answerType1">Yes/No Selection</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input answerType" data-num='{{$key}}' type="radio" name="answerType{{$key}}" id="answerType2" value="Short Answer" {{$survey->type === "Short Answer" ? "checked":""}}>
                                        <label class="form-check-label" for="answerType2">Short Question</label>
                                    </div>
                                    <div class="form-check form-check-inline ">
                                        <input class="form-check-input answerType" data-num='{{$key}}' type="radio" name="answerType{{$key}}" id="answerType3" value="Long Answer" {{$survey->type === "Short Selection" ? "checked":""}}>
                                        <label class="form-check-label" for="answerType3">Short Selection</label>
                                    </div>
                                    <div class="form-check form-check-inline ">
                                        <input class="form-check-input answerType" data-num='{{$key}}' type="radio" name="answerType{{$key}}" id="answerType4" value="Multiple Question" {{$survey->type === "Multiple Question" ? "checked":""}}>
                                        <label class="form-check-label" for="answerType4">Multiple Question*</label>
                                    </div>
                                </div>

                               <div class="form-group row frmFunction choice_form">
                                    <label class="col-sm-2 col-form-label choice_label{{$key}}"><b> Choice* </b></label>
                                    <div class="col-sm-8 choice{{$key}}" >
                                        @foreach($choices as $abc => $choi)
                                            @if ($abc === $survey->id)
                                                @foreach($choi as $key_choice => $choice)
                                                    <input type="hidden" name="choice_id{{$key}}[]" value="{{$choice['id']}}">
                                                    <div class="input-bar">
                                                    <input id="choice1" type="text" name="choice{{$key . $key_choice}}" class="form-control choice0 width-item" value="{{$choice['option']}}">
                                                    <a class="form_status rmChoice" href="{{route('choice.delete', $choice['id'])}}" onclick="return confirm('Are you sure to delete this choice?')"><span class="glyphicon glyphicon-minus-sign iconRm"></span></a>&nbsp
                                                    </div>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        <div class="dynamic_field_{{$key}} choice0"></div>
                                    <button type="button" class="btn choice0 addChoice" id="addChoice" data-id="{{$key}}">Add a new choice </button>
                                    </div>
                                </div>
                                
                                <div class="form-group row frmFunction" >
                                    <label class="col-sm-2 col-form-label" ><b>Answer Required* </b></label>&nbsp
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="required{{$key}}" id="any" value="0" {{$survey->required === 0 ? "checked":""}}>
                                        <label class="form-check-label" for="any">Any</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="required{{$key}}" id="required" value="1" {{$survey->required === 1 ? "checked":""}}>
                                        <label class="form-check-label" for="required">Required</label>
                                    </div>
                                </div>
                                @if($surveys->count()>=2)
                                    <a class="form_status" href="{{route('survey.delete', $survey->id)}}" onclick="return confirm('Are you sure to delete this survey?')" style="font-size:30px"><span class="glyphicon glyphicon-minus-sign"></span></a>
                                @endif
                            </div>
                            @endforeach
                            <div id="newQuestion"></div>

                            <hr class="frmFunction">

                            <div class="form-group row frmFunction" >
                                <label class="col-sm-2 col-form-label frmFunction" ></label>
                                <div class="col-sm-8 frmFunction">
                                        <button type="button" class="btn btn-warning frmFunction" id="addQuestion">Add new question</button>
                                </div>
                            </div>
                            <div class="form-group row frmFunction" >
                                <label class="col-sm-2 col-form-label frmFunction" ><b>Answer deadline* </b></label>
                                <div id="datepicker" class="input-group date datepicker" data-date-format="yyyy-mm-dd"> 
                                    <input class="form-control"  type="text" name="deadline" value="{{$contact->deadline}}" readonly> 
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> 
                                </div>
                            </div>
                            {{-- ---end show survey--- --}}
                        </div>
                    </div>
                </div>
                <input type="hidden" value="1" name="number" class="number">
                <input type="hidden" value="sent" name="sent" >
                <input type="submit" name="submit" value="UPDATE" class="btn btn-primary rpUpdate">
                <a href="{{route('list')}}" class="btn btn-danger rpCancel">CANCEL</a>
            </form>
        </div>
        <!-- Classes List Modal -->
        @include('modals.listClasses')
    </div>    
        <!--student list modal-->
        @include('modals.listStudents')
</div>
@endsection

@section('ajax')
<script src="{{asset('js/radioClick.js')}}"></script>
<script>
    //show classes list by class
    $(document).on('click', '.classesList', function() {
        var classname = $(this).attr('id');
        $.ajax({
            url: '{{ route('ajaxStudent') }}',
            method: 'get',
            data: {
                name: classname,
            },
            success: function(response) { 
                $("#tbody").html("");               
                response.forEach(function(value){
                    studentlist = "<tr class='student" + value.id + "' >"+
                        "<td>" + value.name + "</td>"+
                        "<td>" + value.dob + "</td>"+
                        "<td>" + value.parent + "</td>"+
                        "<td>" + value.parent_email + "</td>"+
                        "<td>" + value.class_name + "</td>"+
                        "<td>" + '<input type="button" id="'+ value.id +'" class="btn btn-primary selectOneStudent" value="Select">' + "</td>"+
                        "</tr>";
                        $("#tbody").append(studentlist);
                }); 
                $('#studentsListModal').modal('show');
                return;
            },
            error: function() {

            }
        });
    });
    // show students
    $(document).on('click', '.selectOneStudent', function(){
        $(this).hide();
        var id_student = $(this).attr('id');
        $.ajax({
            url: '{{ route('SelectOneStudent') }}',
            method: 'get',
            data: {
                id: id_student,
            },
            success: function(response) {               
                response.forEach(function(value){
                    student = '<div class="form-group row select-student selectOne" >'
                    student += '<label class="col-sm-2 col-form-label select-student"></label>'
                    student += '<input type="hidden" name="student_name[]" value="'+value.name+'">'
                    student += '<div class="col-sm-6 "><div  class="select-student ">'+value.name+'</div></div>'
                    student += '<button type="button" class="btn btn-danger btn-removeStudent  select-student" >Delete</button></div>'
                    $(".showStudent").append(student);
                });
                $('#studentsListModal').modal('show');
                return;
            },
            error: function() {

            }
        });
    });

    $(document).on('click','.btn-removeStudent', function(){
        $(this).parent('div').remove();
    });

    //datepicker
    $(function () {
        var date = new Date();
        date.setDate(date.getDate());
        $('.datepicker').datepicker({ 
            startDate: date
        });
    });
</script>
    <script src="{{asset('js/addNewInput.js')}}"></script>
    <script src="{{asset('js/addNewQuestion.js')}}"></script>
@endsection

