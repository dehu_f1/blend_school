<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
        content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <title>@yield('title')</title>

    <base href="{{asset('navigave_layouts')}}/">
    @include('script.script');
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar Holder -->
        @include('particles.navigave_bar')
        <!-- Page Content Holder -->
        <div id="content">

            @include('particles.header')
            <div class="line">
                @yield('content')
            </div>
        </div>
        @yield('script')
    </div>
    @yield('ajax')
</body>

</html>
