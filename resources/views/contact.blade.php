@extends('layouts.master')
@section('title')
        BLEND | Create contact
@endsection

@section('content')
    <link rel="stylesheet" href="css/thanh-style.css">
    <div class="container" >
        <div class="row">
            <div class="col-12">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="post" action="{{ route('addContact') }}" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"><b>Send Target*: </b></label>&nbsp;

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="target" id="target1" checked
                                           value="All students">
                                    <label class="form-check-label" for="target1">All</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="target" id="target2"
                                           value="Grade">
                                    <label class="form-check-label" for="target2">Grades</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="target" id="target3"
                                           value="Individual">
                                    <label class="form-check-label" for="target3">Individual</label>
                                </div>
                            </div>

                            <div class="form-group row grade">
                                <label class="col-sm-2 col-form-label"><b>Grades </b></label>&nbsp;

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="grade" id="grade10"
                                           value="Grade 10">
                                    <label class="form-check-label" for="grade10">Grade 10</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="grade" id="grade11"
                                           value="Grade 11">
                                    <label class="form-check-label" for="grade11">Grade 11</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="grade" id="grade12"
                                           value="Grade 12">
                                    <label class="form-check-label" for="grade12">Grade 12</label>
                                </div>
                            </div>
                            {{--  // select target 10  --}}
                            <div class="form-group row grade selectTarget selection10" id="selectTarget">
                                <label class="col-sm-2 col-form-label selection10"><b>Select Target </b></label>&nbsp;

                                <div class="form-check form-check-inline selection10">
                                    <input class="form-check-input selection10" type="radio" name="selectTarget"
                                           id="allStudentGrade10" value="All Student grade 10">
                                    <label class="form-check-label selection10" for="allStudentGrade10">All Student
                                        grade 10</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input selection10" type="radio" name="selectTarget"
                                           id="HR10" value="HR designation 10">
                                    <label class="form-check-label selection10" for="HR10">HR designation 10</label>
                                </div>
                            </div>

                            {{--  // select target 11  --}}
                            <div class="form-group row grade selectTarget selection11" id="selectTarget">
                                <label class="col-sm-2 col-form-label selection11"><b>Select Target </b></label>&nbsp;

                                <div class="form-check form-check-inline selection11">
                                    <input class="form-check-input selection11" type="radio" name="selectTarget"
                                           id="allStudentGrade11" value="All Student grade 11">
                                    <label class="form-check-label selection11" for="allStudentGrade11">All Student
                                        grade 11</label>
                                </div>
                                <div class="form-check form-check-inline selection11">
                                    <input class="form-check-input selection11" type="radio" name="selectTarget"
                                           id="HR11" value="HR designation 11">
                                    <label class="form-check-label selection11" for="HR11">HR designation 11</label>
                                </div>
                            </div>

                            {{--  // select target 12  --}}
                            <div class="form-group row grade selectTarget selection12" id="selectTarget">
                                <label class="col-sm-2 col-form-label selection12"><b>Select Target </b></label>&nbsp;

                                <div class="form-check form-check-inline selection12">
                                    <input class="form-check-input selection12" type="radio" name="selectTarget"
                                           id="allStudentGrade12" value="All Student grade 12">
                                    <label class="form-check-label selection12" for="allStudentGrade12">All Student
                                        grade 12</label>
                                </div>
                                <div class="form-check form-check-inline selection12">
                                    <input class="form-check-input selection12" type="radio" name="selectTarget"
                                           id="HR12" value="HR designation 12">
                                    <label class="form-check-label selection12" for="HR12">HR designation 12</label>
                                </div>
                            </div>

                            {{--  HR designation 10  --}}
                            <div class="form-group row HRdesignation10">
                                <label class="col-sm-2 col-form-label HRdesignation10"><b>HR designation </b></label>

                                <div class="form-check form-check-inline HRdesignation10">
                                    <input class="form-check-input HRdesignation10" type="checkbox"
                                           name="HRdesignation[]" id="class10A" value="Class 10A">&nbsp;
                                    <label class="form-check-label HRdesignation10" for="class10A">ClassName:
                                        10A </label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation10">
                                    <input class="form-check-input HRdesignation10" type="checkbox"
                                           name="HRdesignation[]" id="class10B" value="Class 10B">
                                    <label class="form-check-label HRdesignation10" for="class10B">ClassName:
                                        10B</label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation10">
                                    <input class="form-check-input HRdesignation10" type="checkbox"
                                           name="HRdesignation[]" id="class10C" value="Class 10C">
                                    <label class="form-check-label HRdesignation10" for="class10C">ClassName:
                                        10C</label>
                                </div>
                            </div>

                            {{--  HR designation 11  --}}
                            <div class="form-group row HRdesignation11">
                                <label class="col-sm-2 col-form-label HRdesignation11"><b>HR designation </b></label>

                                <div class="form-check form-check-inline HRdesignation11">
                                    <input class="form-check-input HRdesignation11" type="checkbox"
                                           name="HRdesignation[]" id="class11A" value="Class 11A">&nbsp;
                                    <label class="form-check-label HRdesignation11" for="class11A">ClassName:
                                        11A </label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation11">
                                    <input class="form-check-input HRdesignation11" type="checkbox"
                                           name="HRdesignation[]" id="class11B" value="Class 11B">
                                    <label class="form-check-label HRdesignation11" for="class11B">ClassName:
                                        11B</label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation11">
                                    <input class="form-check-input HRdesignation11" type="checkbox"
                                           name="HRdesignation[]" id="class11C" value="Class 11C">
                                    <label class="form-check-label HRdesignation11" for="class11C">ClassName:
                                        11C</label>
                                </div>
                            </div>
                            {{--  HR designation 12  --}}
                            <div class="form-group row HRdesignation12">
                                <label class="col-sm-2 col-form-label HRdesignation12"><b>HR designation </b></label>

                                <div class="form-check form-check-inline HRdesignation12">
                                    <input class="form-check-input HRdesignation12" type="checkbox"
                                           name="HRdesignation[]" id="class12A" value="Class 12A">&nbsp;
                                    <label class="form-check-label HRdesignation12" for="class12A">ClassName:
                                        12A </label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation12">
                                    <input class="form-check-input HRdesignation12" type="checkbox"
                                           name="HRdesignation[]" id="class12B" value="Class 12B">
                                    <label class="form-check-label HRdesignation12" for="class12B">ClassName:
                                        12B</label>
                                </div>
                                <div class="form-check form-check-inline HRdesignation12">
                                    <input class="form-check-input HRdesignation12" type="checkbox"
                                           name="HRdesignation[]" id="class12C" value="Class 12C">
                                    <label class="form-check-label HRdesignation12" for="class12C">ClassName:
                                        12C</label>
                                </div>
                            </div>

                            <div class="form-group row select-student" id="select-student">
                                <label class="col-sm-2 col-form-label"><b>Select Student*</b></label>
                                <div class="col-sm-8">
                                    <button type="button" name="select-student" class="btn btn-primary"
                                            data-toggle="modal" data-target="#classesListModal">Select Student
                                    </button>
                                </div>
                            </div>
                            <div class="showStudent"></div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"><b>Date* </b></label>
                                <div id="datepicker" class="input-group date datepicker" data-provide="datepicker"
                                     data-date-format="yyyy-mm-dd">
                                    <input class="form-control input-time" name="date" value='{{ old('date') }}' readonly>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"><b>Title* </b></label>
                                <div class="col-sm-8">
                                    <input id="title" type="text" name="title" value='{{ old('title') }}'
                                           class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"><b>Full Text*</b></label>
                                <div class="col-sm-8">
                                    <textarea name="fulltext" id="fulltext" value='{{ old('fulltext') }}' cols="30"
                                              rows="10" class="form-control" maxlength="250"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"><b>Attach File*</b></label>
                                <div class="col-sm-8">
                                    <div class="input-file-container">
                                        <input class="input-file" id="my-file1" type="file" name="attachfile1">
                                        <label tabindex="0" for="my-file1" class="input-file-trigger">Select a
                                            file...</label>
                                        <p class="file-return"></p>
                                    </div>

                                    <div class="input-file-container">
                                        <input class="input-file" id="my-file2" type="file" name="attachfile2">
                                        <label tabindex="0" for="my-file2" class="input-file-trigger">Select a
                                            file...</label>
                                        <p class="file-return"></p>
                                    </div>

                                    <div class="input-file-container">
                                        <input class="input-file" id="my-file3" type="file" name="attachfile3">
                                        <label tabindex="0" for="my-file3" class="input-file-trigger">Select a
                                            file...</label>
                                        <p class="file-return"></p>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"><b>Form functions </b></label>&nbsp
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="function" checked id="notuse"
                                           value="0">
                                    <label class="form-check-label" for="notuse">Not Uses</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="function" id="use" value="1">
                                    <label class="form-check-label" for="use">Uses</label>
                                </div>
                            </div>
                            <div class="frmFunction" id="frmFunction">
                                <div class="form-group row frmFunction">
                                    <label class="col-sm-2 col-form-label"><b>Question* </b></label>
                                    <div class="col-sm-8">
                                        <input id="question" type="text" name="question0" value='{{ old('question0') }}'
                                               class="form-control" maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group row frmFunction">
                                    <label class="col-sm-2 col-form-label"><b>Answer Type </b></label>&nbsp
                                    <div class="form-check form-check-inline">

                                        <input class="form-check-input answerType" data-num=0 type="radio"
                                               name="answerType0" id="answerType1" value="Yes/No Question">
                                        <label class="form-check-label" for="answerType1">Yes/No Selection</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input answerType" data-num=0 type="radio"
                                               name="answerType0" id="answerType2" value="Short Answer">
                                        <label class="form-check-label" for="answerType2">Short Answer</label>
                                    </div>
                                    <div class="form-check form-check-inline ">
                                        <input class="form-check-input answerType" data-num=0 type="radio"
                                               name="answerType0" id="answerType3" value="Long Answer">
                                        <label class="form-check-label" for="answerType3">Long Answer</label>
                                    </div>
                                    <div class="form-check form-check-inline ">
                                        <input class="form-check-input answerType4" data-num=0 type="radio"
                                               name="answerType0" id="answerType4" value="Multiple Question">
                                        <label class="form-check-label" for="answerType4">Multiple Question*</label>
                                    </div>
                                </div>

                                <div class="form-group row frmFunction choice0">
                                    <label class="col-sm-2 col-form-label choice0"><b> Choice* </b></label>
                                    <div class="col-sm-8 choice0">
                                        <input id="choice1" type="text" name="choice0[]" value='{{ old('choice[]') }}'
                                               class="form-control choice0 width-item" maxlength="50"><br
                                            class="choice0">
                                        <input id="choice2" type="text" name="choice0[]" value='{{ old('choice[]') }}'
                                               class="form-control choice0 width-item" maxlength="50">
                                        <div class="dynamic_field_0 choice0"></div>
                                        <button type="button" class="btn choice0 addChoice" id="addChoice" data-id=0>Add
                                            a new choice
                                        </button>
                                    </div>
                                </div>

                                <div class="form-group row frmFunction">
                                    <label class="col-sm-2 col-form-label"><b>Answer Required* </b></label>&nbsp
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="required0" id="any"
                                               value="0">
                                        <label class="form-check-label" for="any">Any</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="required0" id="required"
                                               value="1">
                                        <label class="form-check-label" for="required">Required</label>
                                    </div>
                                </div>
                            </div>
                            <div id="newQuestion"></div>

                            <hr class="frmFunction">

                            <div class="form-group row frmFunction">
                                <label class="col-sm-2 col-form-label frmFunction"></label>
                                <div class="col-sm-8 frmFunction">
                                    <button type="button" class="btn frmFunction" id="addQuestion">Add new question
                                    </button>
                                </div>
                            </div>
                            <div class="form-group row frmFunction">
                                <label class="col-sm-2 col-form-label frmFunction"><b>Answer deadline* </b></label>
                                <div id="datepicker" class="input-group date datepicker" data-date-format="yyyy-mm-dd">
                                    <input class="form-control input-time" type="text" name="deadline"
                                           value='{{ old('deadline') }}' readonly>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                        <input type="hidden" value="1" name="number" class="number">
                        <input type="hidden" value="0" name="sent" >
                        <input type="submit"  value="SEND" id="summit" class="btn">
                        <label> <img id="loader" src="{{URL::asset('/upload/file/ajax-loader.gif')}}" style="display: none; position: relative; left: 142px; top: -5px;"></label>
                        <a href="{{route('list')}}"  id="cancel" class="btn summit cancel">CANCEL</a>
                    </form>
            </div>
        </div>
    </div>
    <!-- Classes List Modal -->
    <div class="modal fade" id="classesListModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Classes List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ol>
                    @foreach ($classes as $class_name => $classes_list)
                        <li>
                            <button class="btn btn-info classesList" id="{{ $class_name }}"><span>Classes:  {{ $class_name }}</span></button><br>
                        </li>
                    @endforeach
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
    </div>

    <!--student list modal-->
    <div id="studentsListModal" class="modal fade bs-example-modal-lg" tabindex="-1" style="" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="top:180px">
                <div class="modal-header">
                    <h4 class="modal-title" id="classModalLabel">Students List </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <table id="studentTable" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Dob</th>
                            <th>Parents</th>
                            <th>Parent_email</th>
                            <th>Class_name</th>
                            <th>Select Students</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
                    </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>                                   </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('ajax')
    <script>
    $(document).ready(function(){
        $(".grade").hide();
        $(".select-student").hide();

        $("#target1").click(function(){
            $(".grade").hide().find('input[type=radio]').prop('checked',false);
            $(".selectOne").parent().empty();
            $(".select-student").hide().find('input[type=hidden]').val("");
            $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        });

        $("#target2").click(function(){
            $(".grade").show();
            $(".select-student").hide()
            $(".selectOne").parent().empty();
            $(".selection10").hide();
            $(".selection11").hide();
            $(".selection12").hide();

            if($('#grade10').is(":checked")){
                $(".selection11").hide().find('input[type=radio]').prop('checked',false);
                $(".selection12").hide().find('input[type=radio]').prop('checked',false);
                $(".selection10").show();
            }

            if($('#grade11').is(":checked")){
                $(".selection10").hide().find('input[type=radio]').prop('checked',false);
                $(".selection12").hide().find('input[type=radio]').prop('checked',false);
                $(".selection11").show();
            }

            if($('#grade12').is(":checked")){
                $(".selection10").hide().find('input[type=radio]').prop('checked',false);
                $(".selection11").hide().find('input[type=radio]').prop('checked',false);
                $(".selection12").show();
            }
        });
        $("#target3").click(function(){
            $(".select-student").show();
            $(".grade").hide().find('input[type=radio]').prop('checked',false);
            $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        });

        // grade
        $(document).on('click','#grade10', function(){
            $(".selection11").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").hide().find('input[type=radio]').prop('checked',false);
            $(".selection10").show();
            $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);

        });

        $(document).on('click','#grade11', function(){
            $(".selection10").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").hide().find('input[type=radio]').prop('checked',false);
            $(".selection11").show();
            $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        });

        $(document).on('click','#grade12', function(){
            $(".selection10").hide().find('input[type=radio]').prop('checked',false);
            $(".selection11").hide().find('input[type=radio]').prop('checked',false);
            $(".selection12").show();
            $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        });


        $(".frmFunction").hide();
        $("#use").click(function(){
            $(".frmFunction").show();
        });
        $("#notuse").click(function(){
            $(".frmFunction").find('input[type=text]').val('');
            $(".frmFunction").find('input[type=radio]').prop('checked',false);
            $(".frmFunction").hide();
        });

        //HRdesignation
        $('.HRdesignation10').hide();
        $('.HRdesignation11').hide();
        $('.HRdesignation12').hide();
        $(document).on('click', '#allStudentGrade10', function(){
            $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        });
        $(document).on('click', '#allStudentGrade11', function(){
            $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        });
        $(document).on('click', '#allStudentGrade12', function(){
            $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        });
        $(document).on('click', '#HR10', function(){
            $('.HRdesignation10').show();
            $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        });
        $(document).on('click', '#HR11', function(){
            $('.HRdesignation11').show();
            $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation12').hide().find('input[type=checkbox]').prop('checked',false);
        });
        $(document).on('click', '#HR12', function(){
            $('.HRdesignation12').show();
            $('.HRdesignation10').hide().find('input[type=checkbox]').prop('checked',false);
            $('.HRdesignation11').hide().find('input[type=checkbox]').prop('checked',false);
        });


        let num = $(".answerType").data('num');
        $(".choice"+num).hide();
        $(document).on('click', '.answerType', function() {
            let num = $(this).data('num');
            $(".choice"+num).hide().find('input[type=text]').val('');
        });
        $(document).on('click', '.answerType4', function() {
            let num = $(this).data('num');
            $(".choice"+num).show();
        })

        // add new input

        $(document).on('click', '.addChoice', function(){
            let index = $(this).data('id');
            let html = '<div class="input-bar">';
                html += '<br><input type="text" name="choice'+index+'[]"  class="form-control input-bar-item width-item choice-list" maxlength="50">&nbsp';
                html += '<button type="button" class="btn btn-danger input-bar-item btn-remove" id="remove" >X</button>';
                html += '</div>';
            $('.dynamic_field_' + index).append(html);
        });

        $(document).on('click', '.btn-remove', function(){

            $(this).parent('div').remove();
        });

        // add new form function
        var j = $('.number').val();

        $('#addQuestion').click(function(){
            let index = $('.addChoice').length;
            let num = $('.answerType').length;

           let form = '<div><hr class="frmFunction">';
            form +='<div class="frmFunction" id="frmFunction'+j+'"><div class="form-group row frmFunction" >';
            form += '<label class="col-sm-2 col-form-label" ><b>Question* </b></label><div class="col-sm-8">';
            form += '<input id="question'+j+'" type="text" name="question'+j+'" value="{{ old("question'+j+'") }}" class="form-control" ></div></div>';
            form +='<div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Answer Type </b></label>&nbsp';
            form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="answerType'+j+'"  id="answerType1'+j+'" value="Yes/No Question"><label for="answerType1'+j+'" class="form-check-label" >Yes/No Selection</label></div>';
            form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="answerType'+j+'" id="answerType2'+j+'" value="Short Answer" ><label for="answerType2'+j+'" class="form-check-label" >Short Answer</label></div>';
            form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" data-num='+num+' type="radio" name="answerType'+j+'" id="answerType3'+j+'" value="Long Answer"><label for="answerType3'+j+'" class="form-check-label" >Long Answer</label></div>';
            form +='<div class="form-check form-check-inline"><input class="form-check-input answerType4" data-num='+num+' type="radio" name="answerType'+j+'" id="answerType4'+j+'" value="Multiple Question"><label for="answerType4'+j+'" class="form-check-label" >Multiple Question</label></div></div>';
            form +='<div class="form-group row frmFunction choice'+num+'"><label class="col-sm-2 col-form-label choice'+num+'"><b> Choice* </b></label><div class="col-sm-8" ><input id="choice" type="text" name="choice'+j+'[]" value="{{ old("choice'+j+'[]") }}" class="form-control choice'+num+' width-item" maxlength="50"><br><input id="choice2'+j+'" type="text" name="choice'+j+'[]" value="{{ old("choice'+j+'[]") }}" class="form-control choice'+num+' width-item" maxlength="50">';
            form +='<div class="dynamic_field_' + index + ' choice'+num+'"></div><button type="button" class="btn choice'+num+' addChoice" data-id='+index+'>Add a new choice </button></div></div>';
            form +='<div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Answer Required* </b></label>&nbsp<div class="form-check form-check-inline"> <input class="form-check-input" type="radio" name="required'+j+'" id="any'+j+'"  value="0"><label for="any'+j+'" class="form-check-label" >Any</label></div>';
            form +='<div class="form-check form-check-inline"><input class="form-check-input" type="radio" name="required'+j+'" id="required'+j+'" value="1"><label for="required'+j+'" class="form-check-label" >Required</label> </div><button type="button" class="btn frmFunction removeQuestion">X</button></div></div>';
            form +='</div>';
            j++;

            // $('.number').val(j);
            $('#newQuestion').append(form);

            if($('.answerType').is(":checked")){
                $(".choice"+num).hide().find('input[type=text]').val('');
            }else{
                $(".choice"+num).hide().find('input[type=text]').val('');
            }
        });

        $(document).on('click','.removeQuestion', function(){
            num--;
            j--;
            $(this).parent('div').parent('div').parent('div').remove();
        });
    });


    //show classes list by class
    $(document).on('click', '.classesList', function() {
        let classname = $(this).attr('id');
        $.ajax({
            url: '{{ route('ajaxStudent') }}',
            method: 'get',
            data: {
                name: classname,
            },
            success: function(response) {
                $("#tbody").html("");
                response.forEach(function(value){
                   let studentlist = "<tr class='student" + value.id + "' >"+
                        "<td>" + value.name + "</td>"+
                        "<td>" + value.dob + "</td>"+
                        "<td>" + value.parent + "</td>"+
                        "<td>" + value.parent_email + "</td>"+
                        "<td>" + value.class_name + "</td>"+
                        "<td>" + '<input type="button" id="'+ value.id +'" class="btn btn-primary selectOneStudent" value="Select">' + "</td>"+
                        "</tr>";
                        $("#tbody").append(studentlist);
                });
                $('#studentsListModal').modal('show');
                return;
            },
            error: function() {

            }
        });
    });

    // show students
    $(document).on('click', '.selectOneStudent', function(){
        $(this).hide();
        let id_student = $(this).attr('id');
        $.ajax({
            url: '{{ route('SelectOneStudent') }}',
            method: 'get',
            data: {
                id: id_student,
            },
            success: function(response) {
                response.forEach(function(value){
                    let student = '<div class="form-group row select-student selectOne" >'
                        student += '<label class="col-sm-2 col-form-label select-student"></label>'
                        student += '<input type="hidden" name="showStudent[]" value="'+value.name+'">'
                        student += '<div class="col-sm-6 "><div  class="select-student ">'+value.name+'</div></div>'
                        student += '<button type="button" class="btn btn-danger btn-removeStudent  select-student" >Delete</button></div>'
                    $(".showStudent").append(student);
                });
                $('#studentsListModal').modal('show');
                return;
            },
            error: function() {

            }
        });
    });

    $(document).on('click','.btn-removeStudent', function(){
        $(this).parent('div').remove();
    });

    //datepicker
    $(function () {
        let date = new Date();
        date.setDate(date.getDate());
        $('.datepicker').datepicker({
            startDate: date
        });
    });

    //button loading
    $(document).on('click','#submit', function(){
        $("#loader").show();
        $("#cancel").hide();
    });
    </script>
@endsection

