@extends('layouts.master')

@section('title')
BLEND | Contact detail
@endsection

@section('content')
<body>
    <div class="container-fluid">
        <div class="clearfix mt-5">
            <div class="btn-group row ml-1">
                <button type="button" class="btn btn-light float-left">Detail</button>
                <button type="button" class="btn btn-light">Read/Answers</button>
            </div>
            <button type="button" class="btn btn-primary ml-3 float-right"
                onclick="window.location.href='{{route('list')}}'">Back</button>

            <button type="button" class="btn btn-primary float-right "  onclick="window.location.href='{{route('edit', $contacts->id)}}'">Edit</button>
        </div>

        <div class="mt-5 p-3" style="background-color: darkgray">
            Setting
        </div>
        <div class="mt-5">
            <table class="clearfix table table-bordered">
                <thead>
                    <tr>
                        <th class="col-xs-2 col-sm-2 col-md-2 col-lg-3" scope="col">Contact Number</th>
                        <td>{{$contacts->id}}
                            <a href="{{route('destroy', $contacts->id)}}" onclick="return confirm('Are you sure to delete this contact?')"
                            style="margin-left:20%">Delete</a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Date</th>
                        <td>{{Carbon\Carbon::parse($contacts->registerdate)->diffForHumans()}}</td>

                    </tr>
                    <tr>
                        <th scope="col">Title</th>
                        <td>{{$contacts->title}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Send target</th>
                        <td><?php
                        if ($contacts->target == 'All students') {
                            echo($contacts->target);
                        }
                        if (isset($contacts->student_name)) {
                            echo($contacts->student_name);
                        }
                        if (isset($contacts->HR_designation)) {
                            echo($contacts->HR_designation);
                        }
                        if (isset($contacts->grade) && empty($contacts->HR_designation)) {
                            echo($contacts->grade);
                        }
                        ?></td>
                    </tr>
                    <tr>
                        <th scope="col">Status</th>
                        <td>
                            @if($contacts->send_status == 0)
                            Unsent
                            <a href = '{{route('sendMail', ['id' => $contacts->id, 'target' => $contacts->target, 'grade' => $contacts->grade, 'select_grade' => $contacts->select_grade, 'HR_designation' => $contacts->HR_designation, 'student_name' => $contacts->student_name])}}' onclick="return confirm('Would You Like Send Mail ?')" style="margin-left:10%" class="btn btn-primary">Send Mail</a>
                            @else
                            <b>Sent</b>
                            @endif

                        </td>

                    </tr>
                    <tr>
                        <th scope="col">Article</th>
                        <td>{{$contacts->content}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Attach file</th>
                        <td>
                            <?php
                            if(isset($contacts->attachfile1)) {
                                echo "<img style='width:25px' src='img/file.png'> &nbsp;";
                            }
                            if(isset($contacts->attachfile2)){
                                echo "<img style='width:25px' src='img/file.png'> &nbsp;";
                            }
                            if(isset($contacts->attachfile3)){
                                echo "<img style='width:25px' src='img/file.png'>";
                            };
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Register person</th>
                        <td>{{$contacts->register}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Register date</th>
                        <td>{{$contacts->registerdate}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Final Update</th>
                        <td>{{$contacts->updated_at}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Last Modified</th>
                        <td>{{$contacts->updated_at}}</td>
                    </tr>

                </thead>
            </table>
        </div>



        @if($contacts->form_status)
        <div class="mt-5 p-3" style="background-color: darkgray">
            Form contents
        </div>
        <div class="mt-5">
            <table class="clearfix table table-bordered">
                <thead>
                    <tr>
                        <th class="col-xs-2 col-sm-2 col-md-2 col-lg-3" scope="col">Deadline</th>
                        <td>{{$contacts->deadline}}</td>
                    </tr>
                    @foreach($contacts->survey as $survey)
                    @if($survey->type == 'Yes/No Question')
                    <tr>
                        <th scope="col">Question</th>
                        <td>
                            {{$survey->title}} <br>
                            <input type="radio" name="yesno" checked> Yes &nbsp;
                            <input type="radio" name="yesno"> No
                        </td>
                    </tr>
                    @elseif($survey->type == 'Long Answer')
                    <tr>
                        <th scope="col">Question</th>
                        <td>
                            {{$survey->title}} <br>
                            <textarea style="width:40%" colspan="4"> </textarea>
                        </td>
                    </tr>
                    @elseif($survey->type == 'Short Answer')
                    <tr>
                        <th scope="col">Question</th>
                        <td>
                            {{$survey->title}} <br>
                            <input style="width:40%" type="text">
                        </td>
                    </tr>
                    @elseif($survey->type == 'Multiple Question')
                    <tr>
                        <th scope="col">Question</th>
                        <td>
                            {{$survey->title}} <br>                            
                            @foreach($survey->choice as $choices)
                            <input type="checkbox" value="{{$choices->option}}">{{$choices->option}} &nbsp;
                            @endforeach
                        </td>
                    </tr>
                    @endif
                    @endforeach

                </thead>
            </table>
        </div>
        @endif
    </div>
</body>
    @endsection
