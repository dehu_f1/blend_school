@extends('layouts.master')

@section('title')
BLEND | List contact
@endsection

@section('content')
<div class="container">
    <div class="mt-5">
        @if(Session::has('success'))
        <div class="alert alert-success" id='session_delete'>
            <ul>
                <center> {{Session::get('success')}} </center>
            </ul>
        </div>
        @endif
        <form method="get" id="frm-test" action="{{ route('filter') }}">
            @csrf
            <input type="hidden" name="target" value="">
            <div class="col-md-12 btn-group mt-5  float-left clearfix" style="left: -16px; top: -1px;">
                <input type="button" name='' id='alltarget'
                    class="btn btn-light col-xs-4 col-sm-4 col-md-4 col-lg-2 btn-search-target" value="All Students"
                    style="width:95px;{{ (isset($data['target'])&&($data['target']=='All Students'))? 'background-color:black; color:white':'' }}">

                <input type="button" name='' id='grade1' class="btn btn-light col-md-2 btn-search-target"
                    value="Grade 1"
                    style="width:80px; {{ (isset($data['target'])&&($data['target']=='Grade 1'))? 'background-color:black; color:white':'' }}">
                <input type="button" name='' id='grade2' class="btn btn-light col-md-2 btn-search-target"
                    value="Grade 2"
                    style="width:80px; {{ (isset($data['target'])&&($data['target']=='Grade 2'))? 'background-color:black; color:white':'' }}">

                <input type="button" name='' id='grade3' class="btn btn-light col-md-2 btn-search-target"
                    value="Grade 3"
                    style="width:80px; {{ (isset($data['target'])&&($data['target']=='Grade 3'))? 'background-color:black; color:white':'' }}">

                <input type="button" name='' id='private' class="btn btn-light col-md-3 btn-search-target"
                    value="Private"
                    style="width:80px; {{ (isset($data['target'])&&($data['target']=='Private'))? 'background-color:black; color:white':'' }}">
            </div>
            <div id="select-form" class="col-xs-4 col-sm-3 col-md-2 col-lg-1 btn-group mt-5 mb-5 pt-5">
                <select id='send_status' onchange="this.form.submit()" class="form-control" name='send_status'>
                    <option hidden value=''>Status</option>
                    <option value='0' {{ (isset($data['send_status'])&&($data['send_status']==0))? 'selected':'' }}>
                        Unsent
                    </option>
                    <option value='1' {{ (isset($data['send_status'])&&($data['send_status']==1))? 'selected':'' }}>Sent
                    </option>
                </select>

            </div>
        </form>
        <a href="{{route('contact')}}"><button type="button" class="btn btn-primary float-right">Create
                contact</button></a>

    </div>



    <table id="table" class="table table-bordered">
        <thead>
            <tr>
                <th class="col-md-1" scope="col">Code LL</th>
                <th class="col-md-1" scope="col">Date</th>
                <th class="col-md-4" scope="col">Title</th>
                <th class="col-md-1" scope="col">Target</th>
                <th class="col-md-1" scope="col">Status</th>
                <th class="col-md-2" scope="col">Registered person </th>
                <th class="col-md-2" scope="col">Registered Date</th>
            </tr>
        </thead>

        <tbody id="all_contact">
            @foreach($contacts as $contact)
            <tr style="cursor: pointer;" onclick="location.href='{{route('detail',$contact->id)}}'"
                onmouseover="changeColor(this)" onmouseout="changeColor1(this)">
                <td>{{$contact->id}}</th>
                <td>{{$contact->created_at}}</td>
                <td>{{$contact->title}}</td>
                <td>{{$contact->target}}</td>
                <td>@if($contact->send_status == 1)
                    Sent
                    @elseif ($contact->send_status == 0)
                    Unsent
                    @endif
                </td>
                <td>{{$contact->register}}</td>
                <td>{{$contact->registerdate}}</td>
            </tr>
            @endforeach
        </tbody>

    </table>
    <div>
        {{-- <div class="pagination float-right" style="float:right"> --}}
        {{ $contacts->appends(request()->query()) }}
        {{-- </div> --}}
    </div>

    <script>
        $(document).on('click', '.btn-search-target', function () {
            var val = $(this).attr('value');
            $('input[name = "target"]').val(val);
            $('#frm-test').submit();
        })

        function changeColor(x) {
            x.style.color = '#2978BD';
        }

        function changeColor1(x) {
            x.style.color = "black";
        }
        //  Session delete success

            setTimeout(function () {
                $('#session_delete').hide()
            }, 2000);
    </script>
    @endsection
