@section('css')
    <link rel="stylesheet" href="css/navigave_style.css">
@endsection
<nav id="sidebar">
    <div class="sidebar-header">
        <a href="{{route('index')}}" style="text-decoration: none;"><img id='logo' src="img/blend_w_350.png"> </a>
    </div>

    <ul class="list-unstyled components">

        <div id='test'>
            <img id='userlogo' class="img-responsive img-rounded" src="img/user.jpg" alt="User picture">
        </div>
        <div id='test1'>
            <span class="user-name">Toho<strong>Smith</strong>
            </span><br>
            <span class="user-role">Manager</span>
        </div>
        <li class="active">
            <a href="#homeSubmenu7" data-toggle="collapse" aria-expanded="false" style="text-decoration: none;"><img class='category_logo'
                    src="img/icon-bulleted-list-w.png">Attendance </a>
            <ul class="collapse list-unstyled" id="homeSubmenu7">
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 1</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 2</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 3</a></li>
            </ul>
        </li>

        <li class="active">
            <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" style="text-decoration: none;"><img class='category_logo'
                    src="img/icon-book-stack-w.png">Grade</a>
            <ul class="collapse list-unstyled" id="homeSubmenu1">
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 1</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 2</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 3</a></li>
            </ul>
        </li>
        <li class="active">
            <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" style="text-decoration: none;"><img class='category_logo'
                    src="img/icon-school-building-w.png">School</a>
            <ul class="collapse list-unstyled" id="homeSubmenu2">
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 1</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 2</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 3</a></li>
            </ul>
        </li>
        <li class="active">
            <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" style="text-decoration: none;"'><img class='category_logo'
                    src="img/icon-assessment-w.png">Learning</a>
            <ul class="collapse list-unstyled" id="homeSubmenu3">
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 1</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 2</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 3</a></li>
            </ul>
        </li>
        <li class="active">
            <a class='category' href="#homeSubmenu4" data-toggle="collapse" aria-expanded="false" style="text-decoration: none;"><img class='category_logo'
                    src="img/icon-pdf-w2.png">Form</a>
            <ul class="collapse list-unstyled" id="homeSubmenu4">
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 1</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 2</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 3</a></li>
            </ul>
        </li>
        <li class="active">
            <a class='category' href="#homeSubmenu5" data-toggle="collapse" aria-expanded="false" style="text-decoration: none;"> <img class='category_logo'
                    src="img/icon-kentei-w.png">Certification</a>
            <ul class="collapse list-unstyled" id="homeSubmenu5">
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 1</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 2</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 3</a></li>
            </ul>
        </li>
        <li class="active">
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" style="text-decoration: none;"> <img id='category_logo'
                    src="img/icon-communication-w.png">
                Parent Contact</a>
            <ul class="collapse list-unstyled" id="homeSubmenu">
                <li><a href="{{route('list')}}" style="text-decoration: none;">Contact list</a></li>
                <li><a href="{{route('contact')}}" style="text-decoration: none;">Create new</a></li>
            </ul>
        </li>
        <li class="active">
            <a href="#homeSubmenu6" data-toggle="collapse" aria-expanded="false" style="text-decoration: none;"> <img class='category_logo'
                    src="img/icon-memo-w.png">Student notes</a>
            <ul class="collapse list-unstyled" id="homeSubmenu6">
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 1</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 2</a></li>
                <li><a href="{{route('index')}}" style="text-decoration: none;">Home 3</a></li>
            </ul>
        </li>

    </ul>

</nav>
