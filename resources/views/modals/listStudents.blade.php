<div id="studentsListModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="classModalLabel">Students List </h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
            </button>
        </div>
        <div class="modal-body">
            <table id="studentTable" class="table table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Dob</th>
                    <th>Parents</th>
                    <th>Parent_email</th>
                    <th>Class_name</th>
                    <th>Select Students</th>
                </tr>
            </thead>
            <tbody id="tbody">
            </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>                                   </div>
        </div>
    </div>
</div>
</div>