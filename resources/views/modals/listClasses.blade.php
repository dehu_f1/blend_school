<div class="modal fade" id="classesListModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Classes List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">                    
                <ol>
                    @foreach ($classes as $class_name => $classes_list)
                        <li>
                            <button class="btn btn-info classesList" id="{{ $class_name }}"><span>Classes:  {{ $class_name }}</span></button><br>
                        </li>                       
                    @endforeach                        
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>