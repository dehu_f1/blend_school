<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('students')->insert([
                'name' => $faker->name,
                'dob' => $faker->date($format='Y-m-d', $max='now'),
                'parent' => $faker->name,
                'parent_email' => $faker->unique()->email,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'grade' => $faker->randomElement($array = array ('Grade 10','Grade 11','Grade 12')),
                'class_name' => $faker->randomElement($array = array ('Class 10A','Class 10B','Class 10C','Class 11A','Class 11B','Class 11C','Class 12A','Class 12B','Class 12C')),
            ]);
        }
    }
}
