<?php

use Illuminate\Database\Seeder;
use App\Survey;

class ChoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('choices')->insert([
                'option' => $faker->randomElement($array = array ('Yes','No','Bus','satisfied','
                unsatisfied')),
                'survey_id' => Survey::all()->random()->id,
            ]);
        }
    }
}
