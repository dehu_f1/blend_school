<?php

use Illuminate\Database\Seeder;
use App\Contact;

class SurveysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('surveys')->insert([
                'title' => $faker->sentence(),
                'type' => $faker->randomElement($array = array ('Yes/No Question','Short Answer','Long Answer','Multiple Question')),
                'required' => $faker->randomElement($array = array (true,false)),
                'contact_id' => Contact::all()->random()->id,
            ]);
        }
    }
}
