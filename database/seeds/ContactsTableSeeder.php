<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();


        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('contacts')->insert([
                'target' => $faker->randomElement($array = array ('All students','Grade','Individual')),
                'grade' => $faker->randomElement($array = array ('Grade 10','Grade 11','Grade 12')),
                'register' => $faker->name,
                'registerdate' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'title' => $faker->sentence(),
                'content' => $faker->text($maxNbChars = 200),
                'form_status' => $faker->randomElement($array = array (true,false)),
                'send_status' => $faker->randomElement($array = array (true,false)),
                'attachfile1' => $faker->url,
                'attachfile2' => $faker->url,
                'attachfile3' => $faker->url,
                'deadline' => $faker->date($format='Y-m-d', $max='now'),
                'created_at' => $faker->date($format = 'Y-m-d',$min = '2017-01-22', $max = 'now'),
            ]);
        }
    }
}
