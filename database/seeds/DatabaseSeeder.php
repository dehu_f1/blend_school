<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ContactsTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(SurveysTableSeeder::class);
        $this->call(ChoicesTableSeeder::class);
        $this->call(ContactStudentTableSeeder::class);
    }
}
