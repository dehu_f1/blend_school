<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('target')->nullable();
            $table->string('grade')->nullable();
            $table->string('select_grade')->nullable();            
            $table->string('HR_designation')->nullable();
            $table->string('student_name')->nullable();
            $table->string('register')->nullable();
            $table->date('registerdate')->nullable();
            $table->string('title')->nullable();
            $table->string('content',200)->nullable();
            $table->string('form_status')->nullable();
            $table->string('send_status')->nullable();
            $table->string('attachfile1',255)->nullable();
            $table->string('attachfile2',255)->nullable();
            $table->string('attachfile3',255)->nullable();
            $table->date('deadline')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
